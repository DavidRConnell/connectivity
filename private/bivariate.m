function connectionMatrix = bivariate(X, ...
                               modelOrder, ...
                               arEstimator, ...
                               connectionFun)

    if nargin < 3
        arEstimator = 'SLS';
        connectionFun = @(E_solo, E_co) log(E_solo / E_co);
    end

    numRegions = size(X, 2);
    connectionMatrix = zeros(numRegions);
    for i = 1:numRegions
        E_x = arModel(X(:, i), modelOrder, arEstimator);
        for j = 1:numRegions
            if i == j
                continue
            end

            E_xy = arModel(X(:, [i j]), modelOrder, arEstimator);
            connectionMatrix(i, j) = connectionFun(E_x, E_xy);
        end
    end
end
