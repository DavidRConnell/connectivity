function connectionMatrix = kernel(channels, modelOrder, kernelType)
    connectionMatrix = zeros(numRegions);
    duration = size(channels, 1) - modelOrder;
    nChannels = 2;
    indeces = ((1:modelOrder)' + (1:duration)) - 1;
    for i = 1:numRegions
        X = channels(:, i);
        X = X(indeces);

        P = projectionOntoXtilda(X);

        for j = 1:numRegions
            if j == i
                continue
            end

            Y = channels(:, j);
            Y = Y(indeces);
            Z = [X; Y];
            Kprime = Z' * Z;
            Ktilda = Kprime - (P * Kprime) - (Kprime * P) + (P * Kprime * P);

            [t, ~] = eig(Ktilda);
            t = t(:, 1:modelOrder);
            y = channels((modelOrder + 1):end, j);

            connectionMatrix(i, j) = connectivityStrength(t, y);
        end
    end

    function P = projectionOntoXtilda(X)
        K = X' * X;
        [v, ~] = eig(K);

        P = zeros(size(v, 1));
        for pi = size(v, 1):-1:(size(v, 1) - modelOrder + 1)
            P = P + v(:, pi) * v(:, pi)';
        end
    end
end

function delta = connectivityStrength(t, addedChannel)
    R = abs(pearsonCorr(addedChannel, t));
    n = size(t, 1);

    rejectionRegion = ...
        multipleComparisonTTests(0.05, size(R, 2));

    tStat = @(r, n) r .* sqrt((n - 2) ./ (1 - r .^ 2));
    R = R(tStat(R, n) > rejectionRegion);
    delta = sum(R .^ 2);

    function rr = multipleComparisonTTests(alpha, nComparisons)
        alpha = alpha / nComparisons;
        pValue = alpha / 2;
        rr = erfcinv(pValue * 2) * sqrt(2);
    end
end
