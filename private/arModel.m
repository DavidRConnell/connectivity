function E = arModel(X, order, estimator)
    switch estimator
      case 'SLS'
        E = connectivity.recursiveARSolver(X, order);
      case 'armorf'
        E = armorf(X', 1, size(X, 1), order);
      otherwise
        error('MATLAB:UnknownEstimator', 'Estimator %s not known', estimator)
    end
end
