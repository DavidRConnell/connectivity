% Solve for autoregression coefficents with a recursive method. Return the
% Returns the covarance of the AR model error given a set of time series.
% X should be a n x k matrix with n being the duration of the time series and
% k being the number of channels.
% The first channel is the channel to estimate.
function E = recursiveARSolver(X, modelOrder)
    duration = size(X, 1) - modelOrder;
    nChannels = size(X, 2);
    P = eye(modelOrder * nChannels) * 1000;

    indeces = ((1:modelOrder)' + (1:duration)) - 1;
    Y = zeros(modelOrder * nChannels, duration);
    for i = 1:nChannels
        tmp = X(:, i);
        rows = (i - 1) * modelOrder + 1;
        rows = rows:(rows + modelOrder - 1);
        Y(rows, :) = tmp(indeces);
    end

    X = X((modelOrder + 1):end, 1);
    A = zeros(modelOrder * nChannels, 1);
    for n = 1:duration
        P = P - ((P * Y(:, n) * Y(:, n)' * P) / (1 + (Y(:, n)' * P * Y(:, n))));
        err = X(n) - Y(:, n)' * A;
        A = A + (P * Y(:, n) * err);
    end

    err = X - (Y' * A);
    E = (err' * err) / duration;
end
