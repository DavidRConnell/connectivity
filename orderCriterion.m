function score = orderCriterion(p, E, X, method)
    N = size(X, 1);
    k = size(X, 2);

    switch method
      case "AIC"
        score = akaikeCriterion(p, E, N, k);
      case "HQ"
        score = hannanQuinnCriterion(p, E, N, k);
      case "BIC"
        score = schwartzCriterion(p, E, N, k);
      otherwise
        error("MATLAB:NonExistingMethod", "Method %s not a known method", method)
    end

    function aic = akaikeCriterion(p, E, N, k)
        aic = log(det(E)) + (2 * p * k ^ 2) / N;
    end

    function hq = hannanQuinnCriterion(p, E, N, k)
        hq = log(det(E)) + (2 * log(log(N)) * p * k ^ 2) / N;
    end

    function bic = schwartzCriterion(p, E, N, k)
        bic = log(det(E)) + (log(N) * p * k ^ 2) / N;
    end
end
