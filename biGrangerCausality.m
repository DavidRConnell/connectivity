function connectivityModel = biGrangerCausality(X, modelOrder, algorithm, kernelFun)
% TODO: Cleanup pretty forced model.
    if nargin == 3 && strcmp(algorithm, 'kernel')
        kernelFun = 'linear';
    else
        kernelFun = @(E_solo, E_co) log(E_solo / E_co);
    end

    if nargin < 3
        algorithm = 'SLS';
        kernelFun = @(E_solo, E_co) log(E_solo / E_co);
    end

    if strcmp(algorithm, 'kernel')
        connectivityModel = kernel(X, modelOrder, kernelFun);
    else
        connectivityModel = bivariate(X, modelOrder, algorithm, kernelFun);
    end
end
