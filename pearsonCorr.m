function corr = pearson(a, b)
% Expects inputs to be vectors or matrices with variable to correlate over in the
% first dimension.
    a = a - mean(a, 1);
    if nargin == 1
        b = a;
    elseif nargin == 2
        b = b - mean(b, 1);
    else
        error('Too many inputs; can only correlate between two signals.')
    end

    normalizationFactor = (sum(a .^ 2, 1) .^ 0.5)' .* (sum(b .^ 2, 1) .^ 0.5);
    corr = (a' * b) ./ normalizationFactor;
end
