function connectionMatrix = mvGrangerCausality(X, modelOrder, connectionFun)
    numRegions = size(X, 2);
    connectionMatrix = zeros(numRegions);

    nChannels = size(X, 2);
    duration = size(X, 1) - modelOrder;
    totParameters = createParameters(X);
    for i = 1:numRegions
        E_tot = arSolver(X(:, i), totParameters, nChannels);
        for j = 1:numRegions
            if j == i
                % Since self is obviously highly predictive it will mess up scalling.
                continue
            end

            untilJthChannel = 1:((j - 1) * modelOrder);
            afterJthChannel = (j * modelOrder + 1):size(totParameters, 1);

            if j == 1
                notJthChannel = afterJthChannel;
            elseif j == numRegions
                notJthChannel = untilJthChannel;
            else
                notJthChannel = [untilJthChannel, afterJthChannel];
            end

            jthChannelIndeces = (j - 1) * modelOrder + 1;
            E_notx = arSolver(X(:, i), totParameters(notJthChannel, :), ...
                              nChannels - 1);
            connectionMatrix(i, j) = connectionFun(E_notx, E_tot);
        end
    end

    function Y = createParameters(X)
        indeces = ((1:modelOrder)' + (1:duration)) - 1;
        Y = zeros(modelOrder * nChannels, duration);
        for chi = 1:nChannels
            tmp = X(:, chi);
            rows = (chi - 1) * modelOrder + 1;
            rows = rows:(rows + modelOrder - 1);
            Y(rows, :) = tmp(indeces);
        end
    end

    function E = arSolver(X, Y, nChannels)
        P = eye(modelOrder * nChannels) * 1000;
        X = X((modelOrder + 1):end, 1);
        A = zeros(modelOrder * nChannels, 1);
        for n = 1:duration
            P = P - ((P * Y(:, n) * Y(:, n)' * P) / (1 + (Y(:, n)' * P * Y(:, n))));
            err = X(n) - Y(:, n)' * A;
            A = A + (P * Y(:, n) * err);
        end

        err = X - (Y' * A);
        E = (err' * err) / duration;
    end
end
